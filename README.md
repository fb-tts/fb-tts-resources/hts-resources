# Free Tools and Resources for HMM-Based Brazilian Portuguese Speech Synthesis

Documentation for Ericson's [old repository](https://github.com/falabrasil/resources_for_BP_HMM-based_Speech_Synthesis "HMM - Brazilian Portuguese"). It releases a set of natural language processing tools for BP, which expands the already publicly available resources, supporting the development of new researches for academic or industrial purposes. The contributions of this repository are:

- Resources for the training and test stages of an HMM-based TTS system.
- A grapheme-to-phone (G2P) converter with stress mark.
- A rule-based syllabification tool.
- An API that hides from the user the low level details of the engine operation.
 
If you use one of these resources, then please cite the [paper](https://www.researchgate.net/publication/328822051_Free_Tools_and_Resources_for_HMMBased_Brazilian_Portuguese_Speech_Synthesis_16th_Ibero-American_Conference_on_AI_Trujillo_Peru_November_13-16_2018_Proceedings):

	Costa, Ericson & Neto, Nelson. (2018). Free Tools and Resources for 
	HMM-Based Brazilian Portuguese Speech Synthesis:16th Ibero-American 
	Conference on AI, Trujillo, Peru, November 13-16, 2018, Proceedings.
	10.1007/978-3-030-03928-8_30. 
 

## API Methods

The API consists of a Java archive referred to as **UFPAT2S.jar**. This file exposes to the application a set of methods that are described in below. 

| Method                    | Description                    |
|---------------------------|--------------------------------|
| initializeTts()           | Initialize engine              |
| convertTextToSpeechFile() | Synthesize speech from strings |
| speakAudioFile()          | Play the audio file generated  |
| finalizeTts()             | Free engine                    |
